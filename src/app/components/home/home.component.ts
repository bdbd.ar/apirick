import { Component, OnInit } from '@angular/core';
//imporyar mi servico rym
import { RymService } from 'src/app/services/rickandmorty/rym.service';
RymService
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
public personajes:any []=[]; //obtener los personajes

  constructor(private _Rym:RymService) { }

  ngOnInit(): void {
    this.getPersonajes();
  }

  getPersonajes(){
    this._Rym.getPersonajes()
      .subscribe((data:any)=>{
        console.log(data);
        this.personajes=data.results;
      });
  }

}
