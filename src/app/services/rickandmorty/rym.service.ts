//importar http client
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RymService {
  //Url de la api
  public URI='https://rickandmortyapi.com/api';
//extansiar el http cliente
  constructor(private _http:HttpClient) { }

  getPersonajes(){
    const url=`${this.URI}/character`;
    return this._http.get(url)
  }
}
